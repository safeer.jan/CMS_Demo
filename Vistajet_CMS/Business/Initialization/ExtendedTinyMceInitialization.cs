using Vistajet_CMS.Models.Blocks;
using Vistajet_CMS.Models.Pages;
using EPiServer.Cms.TinyMce.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;

namespace Vistajet_CMS.Business.Initialization
{
    [ModuleDependency(typeof(TinyMceInitialization))]
    public class ExtendedTinyMceInitialization : IConfigurableModule
    {
        public void Initialize(InitializationEngine context)
        {
        }

        public void Uninitialize(InitializationEngine context)
        {
        }

        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.Services.Configure<TinyMceConfiguration>(config =>
            {
                // Add content CSS to the default settings.
                config.Default()
                    .ContentCss("/static/css/editor.css");

                // This will clone the default settings object and extend it by
                // limiting the block formats for the MainBody property of an ArticlePage.
                config.For<ArticlePage>(t => t.MainBody)
                    .BlockFormats("Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3");

                // Passing a second argument to For<> will clone the given settings object
                // instead of the default one and extend it with some basic toolbar commands.
                //    config.For<HomeImageTextBlock>(t => t.BottomHeading, config.Empty())
                //        .Plugins(DefaultValues.EpiserverPlugins)
                //        .DisableMenubar()
                //        .Toolbar("formatselect | epi-personalized-content epi-link anchor numlist bullist indent outdent bold italic underline alignleft aligncenter alignright | image epi-image-editor media code | epi-dnd-processor | removeformat | fullscreen");
                //});

                config.For<HomeTextBlock>(t => t.MainBody, config.Empty())
                      .Plugins("epi-link epi-image-editor epi-dnd-processor epi-personalized-content print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help") 
                      .AppendToolbar("color-picker")
                      .Menubar("file edit insert view format table tools help")
                      //.Toolbar("styleselect table merge_cells formatselect | epi-personalized-content epi-link epiimageeditor epiquote anchor numlist bullist indent outdent bold italic underline alignleft aligncenter alignright separator | image epi-image-editor media code | epi-dnd-processor | removeformat | fullscreen code")
                      //.Toolbar("formatselect | epi-personalized-content epi-link anchor numlist bullist indent outdent bold italic underline alignleft aligncenter alignright | image | epi-image-editor | media | code | wordcount | epi-dnd-processor | removeformat | fullscreen")
                      .Toolbar(
                        "epi-link | epi-image-editor | epi-personalized-content | cut copy paste | fullscreen",
                        "styleselect formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | removeformat", "table | toc | codesample")
                      .StyleFormats(
                        new { title = "Bold text", inline = "strong" },
                        new { title = "Red text", inline = "span", styles = new { color = "#ff0000" } },
                        new { title = "Red header", block = "h1", styles = new { color = "#ff0000" } }
                      )
                      .AddSetting("image_caption", true)
                      .AddSetting("image_advtab", true);
                
                   //.Toolbar(
                   //     "epi-link | epi-image-editor | epi-personalized-content | cut copy paste | fullscreen",
                   //     "styleselect formatselect | bold italic strikethrough forecolor backcolor 
                   //        | link | alignleft aligncenter alignright alignjustify
                   //        | numlist bullist outdent indent | removeformat",
                   //     "table | toc | codesample");
                   // 

            });
        }
    }
}

