using EPiServer.Core;

namespace Vistajet_CMS.Models.Pages
{
    public interface IHasRelatedContent
    {
        ContentArea RelatedContentArea { get; }
    }
}
