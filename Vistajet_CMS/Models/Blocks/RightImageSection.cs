﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Vistajet_CMS.Models.Blocks
{
    [ContentType(DisplayName = "RightImageSection", GUID = "88541ad4-ffb0-45fe-bc20-4e0bb35d17fd", Description = "")]
    public class RightImageSection : BlockData
    {
        [CultureSpecific]
        [Editable(true)]
        [Display(
              Name = "Heading",
              Description = "Add a heading.",
              GroupName = SystemTabNames.Content,
              Order = 1)]
        public virtual String Heading { get; set; }



        [CultureSpecific]
        [Editable(true)]
        [Display(
                 Name = "Description",
                 Description = "Add a description.",
                 GroupName = SystemTabNames.Content,
                 Order = 2)]
        public virtual String Description { get; set; }



        [CultureSpecific]
        [Required(AllowEmptyStrings = false)]
        [UIHint(UIHint.Image)]
        [Display(
          Name = "Image", Description = "Add an image (optional)",
          GroupName = SystemTabNames.Content,
          Order = 3)]
        public virtual ContentReference Image { get; set; }
    }
}