﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Vistajet_CMS.Models.Blocks
{
    [ContentType(DisplayName = "SectionHeader", GUID = "6ff2d214-ab83-44f9-b41d-1d95687a35f7", Description = "")]
    public class SectionHeader : BlockData
    {
        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [Display(
          GroupName = SystemTabNames.Content,
          Order = 1)]
        public virtual string FirstLetter { get; set; }



        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [Display(
          GroupName = SystemTabNames.Content,
          Order = 2)]
        public virtual string Heading { get; set; }
    }
}