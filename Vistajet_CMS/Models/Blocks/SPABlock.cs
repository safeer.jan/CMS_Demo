﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Vistajet_CMS.Models.Blocks
{
    [ContentType(DisplayName = "SPABlock", GUID = "0cca2214-2883-4ff7-a7ce-159498580d3b", Description = "")]
    public class SPABlock : BlockData
    {        
        [CultureSpecific]
        [Editable(true)]
        [Display(
                    Name = "Heading",
                    Description = "Heading of SPA Page",
                    GroupName = SystemTabNames.Content,
                    Order = 1)]
                public virtual XhtmlString Heading { get; set; }
     }
}