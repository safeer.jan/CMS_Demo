﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Vistajet_CMS.Models.Blocks
{
    [ContentType(DisplayName = "FleetOffer", GUID = "bab36a0b-de83-4821-adeb-c3edac9c7736", Description = "")]
    public class FleetOffer : BlockData
    {
        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [Display(
          GroupName = SystemTabNames.Content,
          Order = 1)]
        public virtual string Heading { get; set; }



        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [Display(
        GroupName = SystemTabNames.Content,
        Order = 1)]
        public virtual string BottomHeading { get; set; }



        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 2)]
        [UIHint(UIHint.Textarea)]
        public virtual string Text { get; set; }



        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [UIHint(UIHint.Image)]
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 3)]
        public virtual ContentReference Image { get; set; }
    }
}