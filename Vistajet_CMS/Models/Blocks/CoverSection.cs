﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer;
using EPiServer.Web;

namespace Vistajet_CMS.Models.Blocks
{
    [ContentType(DisplayName = "CoverSection", GUID = "c891bdc4-c047-4031-8ebf-5991facd0030", Description = "")]
    public class CoverSection : BlockData
    {
        
        [CultureSpecific]
        [Required(AllowEmptyStrings = false)]
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Heading { get; set; }
        


        [CultureSpecific]
        [Required(AllowEmptyStrings = false)]
        [UIHint(UIHint.Image)]
        [Display(
          GroupName = SystemTabNames.Content,
          Order = 2)]
        public virtual ContentReference Image { get; set; }



        [CultureSpecific]
        [Required(AllowEmptyStrings = false)]
        [UIHint(UIHint.Image)]
        [Display(
         GroupName = SystemTabNames.Content,
         Order = 3)]
        public virtual ContentReference Image2 { get; set; }



        [CultureSpecific]
        [Required(AllowEmptyStrings = false)]
        [UIHint(UIHint.Image)]
        [Display(
         GroupName = SystemTabNames.Content,
         Order = 4)]
        public virtual ContentReference Image3 { get; set; }



        [CultureSpecific]
        [Required(AllowEmptyStrings = false)]
        [UIHint(UIHint.Image)]
        [Display(
           GroupName = SystemTabNames.Content,
           Order = 5)]
        public virtual ContentReference Image4 { get; set; }



        [CultureSpecific]
        [Display(Order = 6, GroupName = SystemTabNames.Content)]
        [Required]
        public virtual string ButtonText { get; set; }


        [CultureSpecific]
        [Display(Order = 7, GroupName = SystemTabNames.Content)]
        [Required]
        public virtual Url ButtonLink { get; set; }
    }
}