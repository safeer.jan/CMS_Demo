﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Vistajet_CMS.Models.Blocks
{
    [ContentType(DisplayName = "HomeTextBlock", GUID = "541fa272-8335-49bf-8964-ea064a2b3b4e",
        Description = "", GroupName =SystemTabNames.Content)]
    [SiteImageUrl]
    public class HomeTextBlock : BlockData
    {
        [CultureSpecific]
        //[Editable(true)]
        //[Required(AllowEmptyStrings = false)]
        [UIHint(UIHint.Textarea)]
        [Display(
           GroupName = SystemTabNames.Content,
           Order = 1)]
        public virtual XhtmlString Heading {
            get; set;
        }



        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string BottomHeading { get; set; }
        //public virtual XhtmlString BottomHeading { get; set; }



        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 2)]
        [UIHint(UIHint.Textarea)]
        public virtual XhtmlString MainBody { get; set; }

        
    }
}