﻿    using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Vistajet_CMS.Models.Blocks
{
    [ContentType(DisplayName = "HomeImageTextBlock", GUID = "663d52a5-dd99-4748-b86f-b178ef3e4f0c", Description = "")]
    public class HomeImageTextBlock : BlockData
    {
        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [UIHint(UIHint.Image)]
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 3)]
        public virtual ContentReference Image { get; set; }


        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [Display(
          GroupName = SystemTabNames.Content,
          Order = 1)]
        public virtual string BottomHeading { get; set; }
    }
}