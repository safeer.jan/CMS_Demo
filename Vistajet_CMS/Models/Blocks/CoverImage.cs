﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Vistajet_CMS.Models.Blocks
{
    [ContentType(DisplayName = "CoverImage", GUID = "9e99232d-dc4c-4748-b5ac-e7ff4f888e3e", Description = "")]
    public class CoverImage : BlockData
    {
        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 2)]
        [UIHint(UIHint.Textarea)]
        public virtual string Text { get; set; }
        

        [CultureSpecific]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        [UIHint(UIHint.Image)]
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 3)]
        public virtual ContentReference Image { get; set; }

    }
}