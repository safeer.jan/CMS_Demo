﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Vistajet_CMS.Models.Blocks
{
    [ContentType(DisplayName = "fleet2", GUID = "c253e8b6-f0c1-4756-b953-6aa808767bb4", Description = "")]
    public class fleet2 : BlockData
    {
       
                [CultureSpecific]
                [Display(
                    Name = "Name",
                    Description = "Name field's description",
                    GroupName = SystemTabNames.Content,
                    Order = 1)]
                public virtual string MainBody { get; set; }
        
    }
}