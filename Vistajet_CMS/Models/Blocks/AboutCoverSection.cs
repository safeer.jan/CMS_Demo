﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Vistajet_CMS.Models.Blocks
{
    [ContentType(DisplayName = "AboutCoverSection", GUID = "11003d74-4e8d-4242-8cd6-77a36950cb88", Description = "")]
    public class AboutCoverSection : BlockData
    {
        [CultureSpecific]
        [Required(AllowEmptyStrings = false)]
        [Display(
          GroupName = SystemTabNames.Content,
          Order = 1)]
        public virtual string Heading { get; set; }



        [CultureSpecific]
        [Required(AllowEmptyStrings = false)]
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 2)]
        [UIHint(UIHint.Textarea)]
        public virtual string Text { get; set; }


        [CultureSpecific]
        [Required(AllowEmptyStrings = false)]
        [UIHint(UIHint.Image)]
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 3)]
        public virtual ContentReference Image { get; set; }
        

    }
}